package unidad4;

import java.util.Random;
import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner teclas = new Scanner(System.in);
		
		Random r = new Random();
		
		//int uno=0, dos=0, tres=0, cuatro=0, cinco=0, seis=0;
		int numeros[] = new int[6];
		
		System.out.println("Introduzca el numero de veces que quiere lanzar el dado");
		
		int tiradas = teclas.nextInt();
		
		for (int i=0;i<tiradas;i++) {
			
			int numero=r.nextInt(6);
			numeros[numero]++;
		}
		
		for (int j=0;j<numeros.length;j++) {
			
			System.out.printf("El numero %d ha salido %d veces \n", j+1, numeros[j]);
		}
		
		teclas.close();
	}

}
