package unidad4;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner teclas = new Scanner(System.in);
		int cont=0;
		
		System.out.println("Introduzca la primera cadena");
		String uno = teclas.nextLine().toLowerCase();
		System.out.println("Introduce la segunda");
		String dos = teclas.nextLine().toLowerCase();
		
		int i=0;
		
		if(uno.length() < dos.length())
			System.out.println("La primera cadena debe ser mayor que la segunda");
		
		else {
		do {
		
			i = uno.indexOf(dos, i);
			if (i != -1) {
				i++;
				cont++;
			}
		}while(i !=-1);
	
		System.out.printf("la segunda esta contenida %d veces en la primera", cont);
		
		}
		
		teclas.close();
	}

}
