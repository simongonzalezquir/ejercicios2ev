package unidad4;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	Scanner teclas = new Scanner(System.in);
	
	System.out.println("Introduzca su DNI completo");
	
	String DNI = teclas.nextLine();
	
	char DNIch = DNI.charAt(DNI.length()-1);
	
	String letra = Character.toString(DNIch).toUpperCase();
	
	String DNINumber=DNI.substring(0, DNI.length()-1);
	
	int numero = Integer.parseInt(DNINumber);
	
	int resto[]=new int[23];
	
	String c[] = new String[23];
	
	c[0]="T";
	c[1]="R";
	c[2]="W";
	c[3]="A";
	c[4]="G";
	c[5]="M";
	c[6]="Y";
	c[7]="F";
	c[8]="P";
	c[9]="D";
	c[10]="X";
	c[11]="B";
	c[12]="N";
	c[13]="J";
	c[14]="Z";
	c[15]="S";
	c[16]="Q";
	c[17]="V";
	c[18]="H";
	c[19]="L";
	c[20]="C";
	c[21]="K";
	c[22]="E";
	
	numero = numero%23;
	
	System.out.println(DNINumber);
	System.out.println(DNIch);
	System.out.println(letra);
	System.out.println(c[numero]);
	
	if (letra == c[numero]) {
		
		System.out.println("Es correcto la letra " + letra + " se corresponde con la letra almacenada " + c[numero]);
	}
	
	else 
		System.out.println("La letra no se corresponde con el DNI");
	
	}
	

}
